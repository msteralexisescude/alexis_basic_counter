﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterBasicLibrary;

namespace BasicCounterTest
{
    [TestClass]
    public class CounterTest1
    {
        [TestMethod]
        public void TestConstructeur()
        {
            CounterLibrary counter = new CounterLibrary(12);
            Assert.AreEqual(12, counter.get());

        }


        [TestMethod]
        public void TestPlus()
        {
            CounterLibrary louis = new CounterLibrary(12);
            louis.plus();
            Assert.AreEqual(13, louis.get());

        }

        [TestMethod]
        public void TestMoins()
        {
            CounterLibrary louis = new CounterLibrary(12);
            louis.moins();
            Assert.AreEqual(11, louis.get());

        }

        [TestMethod]
        public void TestRaz()
        {
            CounterLibrary louis = new CounterLibrary(12);
            louis.raz();
            Assert.AreEqual(0, louis.get());

        }
    }
}
