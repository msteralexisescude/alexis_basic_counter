﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterBasicLibrary
{
    public class CounterLibrary
    {
        //Pour protéger l'intégriter de notre classe on passe l'atribut en Privée
        private int compteur;

        public CounterLibrary(int source = 0)
        // lorsque l'on créera un objet sans lui passer d'entier. On l'initialise alors à zero.
        {
            this.compteur = source;
        }


        public void plus()
        // Permet de 
        {
             this.compteur = this.compteur + 1;
        }

        public void moins()
        //permet d'incrémenter notre objet
        {
             this.compteur = this.compteur - 1;
        }

        public void raz()
         //Permet de remetre a zero notre objet
        {
             this.compteur = 0;
        }

        public int get()
       // puisque notre objet es en privée on n'as besoin d'un get pour pouvoir lire sont contenue.
        {
            return this.compteur;
        }
    }
}
