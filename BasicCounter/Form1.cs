﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CounterBasicLibrary;


namespace BasicCounter
{
    public partial class Form1 : Form
    {
        CounterLibrary counter = new CounterLibrary(0);
        public Form1()
        {
            InitializeComponent();
            affiche.Text = counter.get().ToString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            counter.plus();
            affiche.Text = counter.get().ToString();
        }

        private void Moins_Click(object sender, EventArgs e)
        {
            int size = counter.get();
            if (size>0)
            {
                counter.moins();
                affiche.Text = counter.get().ToString();
            }
            else
            {
                MessageBox.Show("Le compteur ne peut pas être négatif");
            }
            
        }

        private void Raz_Click(object sender, EventArgs e)
        {
            counter.raz();
            affiche.Text = counter.get().ToString();
        }
    }
}
