﻿namespace BasicCounter
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.plus = new System.Windows.Forms.Button();
            this.moins = new System.Windows.Forms.Button();
            this.raz = new System.Windows.Forms.Button();
            this.total = new System.Windows.Forms.Label();
            this.affiche = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(631, 207);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 0;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.Button1_Click);
            // 
            // moins
            // 
            this.moins.Location = new System.Drawing.Point(183, 207);
            this.moins.Name = "moins";
            this.moins.Size = new System.Drawing.Size(75, 23);
            this.moins.TabIndex = 1;
            this.moins.Text = "-";
            this.moins.UseVisualStyleBackColor = true;
            this.moins.Click += new System.EventHandler(this.Moins_Click);
            // 
            // raz
            // 
            this.raz.Location = new System.Drawing.Point(388, 314);
            this.raz.Name = "raz";
            this.raz.Size = new System.Drawing.Size(75, 23);
            this.raz.TabIndex = 2;
            this.raz.Text = "raz";
            this.raz.UseVisualStyleBackColor = true;
            this.raz.Click += new System.EventHandler(this.Raz_Click);
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Location = new System.Drawing.Point(411, 112);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(27, 13);
            this.total.TabIndex = 3;
            this.total.Text = "total";
            // 
            // affiche
            // 
            this.affiche.AutoSize = true;
            this.affiche.Location = new System.Drawing.Point(410, 207);
            this.affiche.Name = "affiche";
            this.affiche.Size = new System.Drawing.Size(13, 13);
            this.affiche.TabIndex = 4;
            this.affiche.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.affiche);
            this.Controls.Add(this.total);
            this.Controls.Add(this.raz);
            this.Controls.Add(this.moins);
            this.Controls.Add(this.plus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button moins;
        private System.Windows.Forms.Button raz;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label affiche;
    }
}

